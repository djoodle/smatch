# smatch
[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/djoodle/smatch)

V port of: https://github.com/tidwall/match - A simple string pattern matcher

smatch is a very simple pattern matcher where '*' matches on any 
number characters and '?' matches on any one character.

Installing
----------
```
vpkg get https://gitlab.com/djoodle/smatch
```

Example
-------
```
smatch.smatch("hello", "*llo") 
smatch.smatch("jello", "?ello") 
smatch.smatch("hello", "h*o") 
```

Contact
-------
Dan Udell [@djoodle]

License
-------
smatch source code is available under the MIT [License](/LICENSE).