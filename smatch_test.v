import (
	smatch
	modules.vtest
	benchmark
	term
)

const (
	// BMARK_OPS is the number of iterations of the benchmarks before
	// computing ns/ops
	BMARK_OPS=1000000
)

fn test_smatch_quick() {
	assert vtest.assert_true(smatch.smatch("hello world", "hello world"))
	assert vtest.assert_false(smatch.smatch("hello world", "jello world"))
	assert vtest.assert_true(smatch.smatch("hello world", "hello*"))
	assert vtest.assert_false(smatch.smatch("hello world", "jello*"))
	assert vtest.assert_true(smatch.smatch("hello world", "hello?world"))
	assert vtest.assert_false(smatch.smatch("hello world", "jello?world"))
	assert vtest.assert_false(smatch.smatch("hello world", "jello world"))
	assert vtest.assert_true(smatch.smatch("hello world", "hello*"))
	assert vtest.assert_false(smatch.smatch("hello world", "jello*"))
	assert vtest.assert_true(smatch.smatch("hello world", "hello?world"))
	assert vtest.assert_false(smatch.smatch("hello world", "jello?world"))
	assert vtest.assert_true(smatch.smatch("hello world", "he*o?world"))
	assert vtest.assert_true(smatch.smatch("hello world", "he*o?wor*"))
	assert vtest.assert_true(smatch.smatch("hello world", "he*o?*r*"))
	assert vtest.assert_true(smatch.smatch("的情况下解析一个", "*"))
	assert vtest.assert_true(smatch.smatch("的情况下解析一个", "*况下*"))
	assert vtest.assert_true(smatch.smatch("的情况下解析一个", "*况?*"))
	assert vtest.assert_true(smatch.smatch("的情况下解析一个", "的情况?解析一个"))
}

// SMatchTest struct to represent an individual
// test case
struct SMatchTest {
	p string
	s string
	expect bool
}

// test_smatch - these tests validate the logic of wild card matching.
// smatch supports '*' and '?' wildcards, these test cases are more complex/
// real world examples of this matching.
fn test_smatch() {
	tests := [
		// Test case - 1.
		// Test case with pattern containing key name with a prefix. Should accept the same text without a "*".
		SMatchTest{
			p: "my-folder/oo*",
			s: "my-folder/oo",
			expect: true
		},
		// Test case - 2.
		// Test case with "*" at the end of the pattern.
		SMatchTest{
			p: "my-folder/In*",
			s: "my-folder/India/Karnataka/",
			expect: true
		},
		// Test case - 3.
		// Test case with prefixes shuffled.
		// This should fail.
		SMatchTest{
			p: "my-folder/In*",
			s: "my-folder/Karnataka/India/",
			expect: false
		},
		// Test case - 4.
		// Test case with text expanded to the wildcards in the pattern.
		SMatchTest{
			p: "my-folder/In*/Ka*/Ban",
			s: "my-folder/India/Karnataka/Ban",
			expect: true
		},
		// Test case - 5.
		// Test case with the  keyname part is repeated as prefix several times.
		// This is valid.
		SMatchTest{
			p: "my-folder/In*/Ka*/Ban",
			s: "my-folder/India/Karnataka/Ban/Ban/Ban/Ban/Ban",
			expect: true
		},
		// Test case - 6.
		// Test case to validate that `*` can be expanded into multiple prefixes.
		SMatchTest{
			p: "my-folder/In*/Ka*/Ban",
			s: "my-folder/India/Karnataka/Area1/Area2/Area3/Ban",
			expect: true
		},
		// Test case - 7.
		// Test case to validate that `*` can be expanded into multiple prefixes.
		SMatchTest{
			p: "my-folder/In*/Ka*/Ban",
			s: "my-folder/India/State1/State2/Karnataka/Area1/Area2/Area3/Ban",
			expect: true
		},
		// Test case - 8.
		// Test case where the keyname part of the pattern is expanded in the text.
		SMatchTest{
			p: "my-folder/In*/Ka*/Ban",
			s: "my-folder/India/Karnataka/Bangalore",
			expect: false
		},
		// Test case - 9.
		// Test case with prefixes and wildcard expanded for all "*".
		SMatchTest{
			p: "my-folder/In*/Ka*/Ban*",
			s: "my-folder/India/Karnataka/Bangalore",
			expect: true
		},
		// Test case - 10.
		// Test case with keyname part being a wildcard in the pattern.
		SMatchTest{
			p: "my-folder/*",
			s: "my-folder/India",
			expect: true
		},
		// Test case - 11.
		SMatchTest{
			p: "my-folder/oo*",
			s: "my-folder/odo",
			expect: false
		},
		// Test case with pattern containing wildcard '?'.
		// Test case - 12.
		// "my-folder?/" expect "my-folder1/", "my-folder2/", "my-folder3" etc...
		// doesn't match "myfolder/".
		SMatchTest{
			p: "my-folder?/abc*",
			s: "myfolder/abc",
			expect: false
		},
		// Test case - 13.
		SMatchTest{
			p: "my-folder?/abc*",
			s: "my-folder1/abc",
			expect: true
		},
		// Test case - 14.
		SMatchTest{
			p: "my-?-folder/abc*",
			s: "my--folder/abc",
			expect: false
		},
		// Test case - 15.
		SMatchTest{
			p: "my-?-folder/abc*",
			s: "my-1-folder/abc",
			expect: true
		},
		// Test case - 16.
		SMatchTest{
			p: "my-?-folder/abc*",
			s: "my-k-folder/abc",
			expect: true
		},
		// Test case - 17.
		SMatchTest{
			p: "my??folder/abc*",
			s: "myfolder/abc",
			expect: false
		},
		// Test case - 18.
		SMatchTest{
			p: "my??folder/abc*",
			s: "my4afolder/abc",
			expect: true
		},
		// Test case - 19.
		SMatchTest{
			p: "my-folder?abc*",
			s: "my-folder/abc",
			expect: true
		},
		// Test case 20-21.
		// '?' expect '/' too. (works with s3).
		// This is because the namespace is considered flat.
		// "abc?efg" expect both "abcdefg" and "abc/efg".
		SMatchTest{
			p: "my-folder/abc?efg",
			s: "my-folder/abcdefg",
			expect: true
		},
		SMatchTest{
			p: "my-folder/abc?efg",
			s: "my-folder/abc/efg",
			expect: true
		},
		// Test case - 22.
		SMatchTest{
			p: "my-folder/abc????",
			s: "my-folder/abc",
			expect: false
		},
		// Test case - 23.
		SMatchTest{
			p: "my-folder/abc????",
			s: "my-folder/abcde",
			expect: false
		},
		// Test case - 24.
		SMatchTest{
			p: "my-folder/abc????",
			s: "my-folder/abcdefg",
			expect: true
		},
		// Test case 25-26.
		// test case with no '*'.
		SMatchTest{
			p: "my-folder/abc?",
			s: "my-folder/abc",
			expect: false
		},
		SMatchTest{
			p: "my-folder/abc?",
			s: "my-folder/abcd",
			expect: true
		},
		SMatchTest{
			p: "my-folder/abc?",
			s: "my-folder/abcde",
			expect: false
		},
		// Test case 27.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnop",
			expect: false
		},
		// Test case 28.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnopqrst/mnopqr",
			expect: true
		},
		// Test case 29.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnopqrst/mnopqrs",
			expect: true
		},
		// Test case 30.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnop",
			expect: false
		},
		// Test case 31.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnopq",
			expect: true
		},
		// Test case 32.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnopqr",
			expect: true
		},
		// Test case 33.
		SMatchTest{
			p: "my-folder/mnop*?and",
			s: "my-folder/mnopqand",
			expect: true
		},
		// Test case 34.
		SMatchTest{
			p: "my-folder/mnop*?and",
			s: "my-folder/mnopand",
			expect: false
		},
		// Test case 35.
		SMatchTest{
			p: "my-folder/mnop*?and",
			s: "my-folder/mnopqand",
			expect: true
		},
		// Test case 36.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mn",
			expect: false
		},
		// Test case 37.
		SMatchTest{
			p: "my-folder/mnop*?",
			s: "my-folder/mnopqrst/mnopqrs",
			expect: true
		},
		// Test case 38.
		SMatchTest{
			p: "my-folder/mnop*??",
			s: "my-folder/mnopqrst",
			expect: true
		},
		// Test case 39.
		SMatchTest{
			p: "my-folder/mnop*qrst",
			s: "my-folder/mnopabcdegqrst",
			expect: true
		},
		// Test case 40.
		SMatchTest{
			p: "my-folder/mnop*?and",
			s: "my-folder/mnopqand",
			expect: true
		},
		// Test case 41.
		SMatchTest{
			p: "my-folder/mnop*?and",
			s: "my-folder/mnopand",
			expect: false
		},
		// Test case 42.
		SMatchTest{
			p: "my-folder/mnop*?and?",
			s: "my-folder/mnopqanda",
			expect: true
		},
		// Test case 43.
		SMatchTest{
			p: "my-folder/mnop*?and",
			s: "my-folder/mnopqanda",
			expect: false
		},
		// Test case 44.
		SMatchTest{
			p: "my-?-folder/abc*",
			s: "my-folder/mnopqanda",
			expect: false
		}
	]
	// Iterating over the test cases, call the function under test and asert the output.
	for t in tests {
		assert vtest.assert_bool(t.expect, smatch.smatch(t.s, t.p))
	}
}

fn test_is_pattern() {
	patterns := [
		"*", "hello*", "hello*world", "*world",
		"?", "hello?", "hello?world", "?world"
	]
	nonpatterns := [
		"", "hello"
	]
	for p in patterns {
		assert vtest.assert_true(smatch.is_pattern(p))
	}

	for s in nonpatterns {
		assert vtest.assert_false(smatch.is_pattern(s))
	}
}

fn bmark_smatch(tname, str, pattern string, ops int) {
	mut bmark := benchmark.new_benchmark()
	bmark.set_total_expected_steps(ops)
	bmark.verbose = false
	for bmark.ntotal < bmark.nexpected_steps {
		bmark.step()
		if !smatch.smatch(str, pattern) {
			bmark.fail() // call this if the step failed
			println(bmark.step_message('failure'))
		}
		bmark.ok() // call this when the step succeeded
	}
	bmark.stop() // call when you want to finalize the benchmark
	peropns := i16(f64(bmark.total_duration())/ops*1000000)
	println(bmark.total_message('\t$tname benchmark completed ${peropns}ns/op'))
}

fn test_benchmarks() {
	println(term.h_divider())
	println('running benchmarks...')
	bmark_smatch('ASCII', 'hello', 'hello', BMARK_OPS)
	bmark_smatch('unicode', 'hello', 'hello', BMARK_OPS)
	bmark_smatch('with *', 'hello', 'he*', BMARK_OPS)
	bmark_smatch('with ?', 'hello', 'he?lo', BMARK_OPS)
	println(term.h_divider())
}