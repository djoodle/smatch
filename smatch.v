// SMatch provides a simple pattern matcher with unicode support
module smatch

import encoding.utf8

const (
	// ERROR_RUNE is returned when a rune can't be
	// decoded from a string.
	ERROR_RUNE = 0xFFFD
)

// smatch returns true if the str matches pattern. This is 
// a simple wildcard match where '*' matches on any number
// of characters and '?' matches on any one character.
// pattern:
// 	{ term }
// term:
// 	'*'         matches any sequence of non-Separator characters
// 	'?'         matches any single non-Separator character
// 	c           matches character c (c != '*', '?', '\\')
// 	'\\' c      matches character c
//
pub fn smatch(str, pattern string) bool {
	return (pattern == '*') || 
		(pattern == '?' && str.len == 1) || 
		deepmatch(str, pattern)
}

// LOGIC --------------------------------------
// -------------------------------------------

// deepmatch is the main logic to compare bytes in
// a string, to see if they match (either are equal
// or match a wildcard) - or if they are multi-byte
// unicode characters. 
fn deepmatch(str, ptrn string) bool {

	if ptrn == '*' {
		return true
	}

	slen := str.len
	for idx, b in ptrn {
		if b > 0x7f { // Multibyte utf8 char
			return deepmatch_rune(str[idx..], ptrn[idx..])
		}
		if b == `*` {
			return deepmatch(str[idx..], ptrn[idx+1..]) ||
				(slen >= idx+1 && deepmatch(str[idx+1..], ptrn[idx..]))
		} else if b == `?` {
			if slen == idx {
				return false
			}
		} else {
			if slen == idx {
				return false
			}
			if str[idx] > 0x7f {
				return deepmatch_rune(str[idx..], ptrn[idx..])
			}
			if str[idx] != b {
				return false
			}
		}
		if slen == idx+1 && ptrn.len == idx+1 {
			return true
		}
	}
	return ptrn.len == 0 && slen == 0
}

// deepmatch_rune scans runes (utf8 codepoints) rather than bytes
// for a match
fn deepmatch_rune(str, ptrn string) bool {

	if ptrn == "*" {
		return true
	}

	mut pr, mut prsz := get_rune(ptrn, 0)
	mut sr, mut srsz := get_rune(str, 0)
	mut sidx := 0
	mut pidx := 0

 	for pr != ERROR_RUNE {
		if pr == `*` {
			return deepmatch_rune(str[sidx..], ptrn[pidx+prsz..]) ||
				(srsz > 0 && deepmatch_rune(str[sidx+srsz..], ptrn[pidx..]))
		} else if pr == `?` {
			if srsz == 0 {
				return false
			}
		}
		else {
			if srsz == 0 {
				return false
			}
			if sr != pr {
				return false
			}
		}
		pidx+=prsz
		sidx+=srsz
		pr, prsz = get_rune(ptrn, pidx)
		sr, srsz = get_rune(str, sidx)
	}
	return srsz == 0 && prsz == 0
}

// HELPERS -----------------------------------
// -------------------------------------------

// get_rune returns a rune from a point within a string
fn get_rune(s string, idx int) (int, int) {
	if s[idx..].len > 0 {
		if s[idx] > 0x7f {
			r := utf8.get_uchar(s, idx)
			return r, bytes_in_rune_starting(s[idx])
		} else {
			return int(s[idx]), 1
		}
	} else {
		return ERROR_RUNE, 0
	}
}

// is_pattern takes a string and retruns true if it represents a
// wildcard pattern (i.e. it contains one or more `*`s or `?`s)
pub fn is_pattern(str string) bool {
	for i in str {
		if i == `*` || i == `?` {
			return true
		}
	}
	return false
}

// bytes_in_rune_starting looks at the first byte of a utf8 character
// to determine howmany bytes are in its representation.
fn bytes_in_rune_starting(b byte) int {
	return ((( 0xe5000000 >> (( b >> 3 ) & 0x1e )) & 3 ) + 1)
}
